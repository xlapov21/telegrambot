package telegrambot;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import telegrambot.bot.helpers.TextListExtractor;

import static telegrambot.bot.Bot.getBot;

/**
 * Класс входа
 */
public class App {

    /**
     * Метод - точка входа.
     * Коннект экземпляра Bot с приложением Telegram
     * Вывод основной информации из БД
     *
     * @param args входной массив строк
     */
    public static void main(String[] args) {
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            botsApi.registerBot(getBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

        System.out.format(
                "\n\n!!!!Bot Started!!!!\n\n%s\n\n%s\n\n%s\n\nЖурнал запросов:\n",
                TextListExtractor.getGroupsTextList("ru"),
                TextListExtractor.getAllUsersTextList("ru"),
                TextListExtractor.getAdminsTextList("ru")
        );
    }
}
