package telegrambot.bot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import telegrambot.bot.configs.Redirector;

import static telegrambot.bot.helpers.MessageResender.getMessageText;
import static telegrambot.bot.lib.EnvVarSelector.getEnvVar;

/**
 * Класс-обработчик поступающих к боту сообщений.
 */
public class Bot extends TelegramLongPollingBot {

    /**
     * экземпляр Bot
     */
    private static Bot singletonBot;

    /**
     * Закрытый конструктор класса Bot
     */
    private Bot() {
    }

    /**
     * Метод получения синглтон - экземпляра Bot
     *
     * @return синглтон - экземпляр Bot
     */
    public static Bot getBot() {
        if (singletonBot == null) {
            singletonBot = new Bot();
        }
        return singletonBot;
    }

    /**
     * Геттер имени бота
     *
     * @return имя бота
     */
    @Override
    public String getBotUsername() {
        return getEnvVar("ITCOSIBOT_NAME");
    }

    /**
     * Геттер токена бота
     *
     * @return токен
     */
    @Override
    public String getBotToken() {
        return getEnvVar("ITCOSIBOT_TOKEN");
    }

    /**
     * Метод-обработчик обновлений в чатах
     *
     * @param update объект, содержащий информацию о входящем сообщении
     */
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            System.out.format("%s %s (%s - %s) - %s\n",
                    message.getFrom().getLastName(),
                    message.getFrom().getFirstName(),
                    "@" + message.getFrom().getUserName(),
                    message.getChatId(),
                    getMessageText(message)
            );

            if (message.hasText() && message.getText().charAt(0) == '/') {
                Redirector.directCommand(message);
            } else {
                Redirector.directAction(message);
            }
        }
    }
}
