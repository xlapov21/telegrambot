package telegrambot.bot.controllers.concerns;

import org.telegram.telegrambots.meta.api.objects.Message;

/**
 * Интерфейс для контроллеров - действий
 */
public interface Actionable {


    /**
     * Дефолтный метод получения названия действия
     *
     * @return название действия, тождественное названию класса, реализующего интерфейс
     */
    default String getTitle() {
        return this.getClass().getSimpleName();
    }

    /**
     * Метод основного действия. Точка входа в контроллер
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    void doSmth(Message message);

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    boolean onlyText();

}
