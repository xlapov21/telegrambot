package telegrambot.bot.controllers.concerns;

import org.telegram.telegrambots.meta.api.objects.Message;

/**
 * Интерфейс для контроллеров - команд
 */
public interface Commandable {

    /**
     * Метод основного действия. Точка входа в контроллер
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    void doSmth(Message message);

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    boolean forAdmin();
}
