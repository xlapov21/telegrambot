package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;

import static telegrambot.bot.helpers.TextListExtractor.getUsersAndGroupsTextList;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-команда для добавления пользователя в группу
 */
public class AddUserToGroupCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/add_user_to_group";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {return true;}

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет список пользователей и групп, инструкцию и переводит текущее действие в "AddUserToGroup"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        User.updateCurrentAction(chatId, "AddUserToGroup");
        sendText(chatId, getUsersAndGroupsTextList(user.getLanguage()));
        sendText(chatId, getTextByUser(user, "commands/add_user_to_group/instruction"));
    }
}
