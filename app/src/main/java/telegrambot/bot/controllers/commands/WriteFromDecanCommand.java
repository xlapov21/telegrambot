package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByLanguage;
import static telegrambot.bot.helpers.TextListExtractor.getUsersAndGroupsTextList;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-команда для отправки сообщения от лица деканата
 */
public class WriteFromDecanCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/write_from_decan";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет список пользователей и групп, инструкцию и переводит текущее действие в "WriteMsgToUser"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        String language = User.findByChatId(chatId).getLanguage();
        sendText(chatId, getUsersAndGroupsTextList(language) +
                getTextByLanguage(language, "commands/write_from_decan/all_groups") +
                getTextByLanguage(language, "commands/write_from_decan/instruction")
        );
        User.updateCurrentAction(chatId, "WriteMsgToUser");
    }
}
