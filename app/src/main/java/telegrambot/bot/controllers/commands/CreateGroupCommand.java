package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import static telegrambot.bot.views.MessageSender.sendText;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;

import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.helpers.TextListExtractor;
import telegrambot.bot.models.User;

/**
 * Класс контроллер-команда для создания группы
 */
public class CreateGroupCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/create_group";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет список групп, инструкцию и переводит текущее действие в "CreateGroup"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        User.updateCurrentAction(chatId, "CreateGroup");
        sendText(chatId, TextListExtractor.getGroupsTextList(user.getLanguage()) +
                getTextByUser(user, "commands/create_group/instruction")
        );
    }
}
