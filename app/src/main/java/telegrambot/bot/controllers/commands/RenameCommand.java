package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;

import static telegrambot.bot.views.MessageSender.sendText;
import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.models.User.updateCurrentAction;

/**
 * Класс контроллер-команда для смены имени и фамилии пользователя в БД
 */
public class RenameCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/rename";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет инструкцию и переводит текущее действие в "SetName"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        updateCurrentAction(chatId, "SetName");
        sendText(chatId, getTextByChatId(chatId, "commands/rename/instruction"));
    }
}
