package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.helpers.TextListExtractor.getFilesTextList;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-команда для получения закрепленных файлов
 */
public class GetFileCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/get_file";


    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет список закрепленных файлов, инструкцию и переводит текущее действие в "GetFile"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        sendText(chatId, getFilesTextList(user.getLanguage()));
        sendText(chatId, getTextByUser(user, "commands/get_file/instruction"));
        User.updateCurrentAction(chatId, "GetFile");
    }
}
