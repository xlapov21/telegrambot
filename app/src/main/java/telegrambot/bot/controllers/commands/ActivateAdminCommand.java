package telegrambot.bot.controllers.commands;

import static telegrambot.bot.helpers.TextListExtractor.getNotAdminsTextList;
import static telegrambot.bot.views.MessageSender.sendText;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;
import org.telegram.telegrambots.meta.api.objects.Message;

/**
 * Класс контроллер-команда для активации прав администратора у пользователя
 */
public class ActivateAdminCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/activate_admin";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет список пользователей без прав администратора, инструкцию и переводит текущее действие в "ActivateAdmin"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        sendText(chatId, getNotAdminsTextList(user.getLanguage()));
        sendText(chatId, getTextByUser(user, "commands/activate_admin/instruction"));
        User.updateCurrentAction(chatId, "ActivateAdmin");
    }
}