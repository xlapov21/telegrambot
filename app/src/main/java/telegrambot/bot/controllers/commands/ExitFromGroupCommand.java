package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.helpers.TextListExtractor.getUserGroupsTextList;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-команда для выхода пользователя из группы
 */
public class ExitFromGroupCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/exit_from_group";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет список групп, в которых состоит пользователь, инструкцию и переводит текущее действие в "ExitFromGroup"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        sendText(chatId, getUserGroupsTextList(user.getLanguage(), user.getId()));
        sendText(chatId, getTextByUser(user, "commands/exit_from_group/instruction"));
        User.updateCurrentAction(chatId, "ExitFromGroup");
    }
}
