package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.configs.Redirector;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.helpers.FieldGetter.getCommandName;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-команда для вывода текущего действия и доступных команд
 */
public class HelpCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/help";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет текущее действие и доступные команды
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        boolean isAdmin = user.isAdmin();
        StringBuilder outMessageText = new StringBuilder(String.format(
                getTextByUser(user, "commands/help"),
                user.getCurrentAction()
        ));
        for (Commandable command : Redirector.getCommands()) {
            String commandName = getCommandName(command);
            if (!command.forAdmin() || isAdmin) {
                outMessageText.append(commandName).append("\n");
            }
        }
        sendText(chatId, outMessageText.toString());
    }
}
