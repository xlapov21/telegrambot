package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByLanguage;
import static telegrambot.bot.helpers.TextListExtractor.getGroupsTextList;
import static telegrambot.bot.helpers.TextListExtractor.getVisibleGroupsTextList;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-команда для входа в группу
 */
public class AllowEntryGroupCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private static final String COMMAND_NAME = "/allow_entry_group";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет список групп, инструкцию и переводит текущее действие в "AllowEntryGroup"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        String language = user.getLanguage();
        String groupsTextList;
        if (user.isAdmin()) {
            groupsTextList = getGroupsTextList(language);
        } else {
            groupsTextList = getVisibleGroupsTextList(language);
        }
        sendText(chatId, groupsTextList);
        sendText(chatId, getTextByLanguage(language, "commands/allow_entry_group/instruction"));
        User.updateCurrentAction(chatId, "AllowEntryGroup");
    }
}
