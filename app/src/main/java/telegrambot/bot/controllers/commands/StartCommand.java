package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-команда для начала общения с ботом или сброса действия
 */
public class StartCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/start";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Если пользователь был в БД, то переводит его действие в "Waiting".
     * Если пользователь отсутствовал в БД, сохраняет его с действием "SetName"
     * и отправляет инструкцию для вставки имени
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        String outMessageCode;
        User user = User.findByChatId(chatId);
        if (user == null) {
            user = new User(
                    -1,
                    null,
                    null,
                    message.getFrom().getUserName(),
                    chatId,
                    false,
                    "SetName",
                    "ru"
            );
            sendText(chatId, String.format(
                    getTextByUser(user, "commands/start/greeting"),
                    message.getFrom().getFirstName()
            ));
            if (user.save()) {
                outMessageCode = "commands/rename/instruction";
            } else {
                outMessageCode = "commands/start/error_in_db";
            }
        } else if (user.getFirstName().equals("null")) {
            outMessageCode = "commands/rename/instruction";
            User.updateCurrentAction(chatId, "SetName");
        } else {
            outMessageCode = "commands/start/greeting/repeated";
            User.updateCurrentAction(chatId, "Waiting");
        }
        sendText(chatId, getTextByUser(user, outMessageCode));
    }
}
