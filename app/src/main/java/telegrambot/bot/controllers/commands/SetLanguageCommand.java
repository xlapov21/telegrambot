package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.Language;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-команда для смены языка
 */
public class SetLanguageCommand implements Commandable {

    /**
     * Константа - текст сообщения для вызова команды
     */
    private final static String COMMAND_NAME = "/set_language";

    /**
     * Метод определения доступности команды
     *
     * @return булевое значение, является ли команда доступной только для администраторов
     */
    @Override
    public boolean forAdmin() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет список доступных языков, инструкцию и переводит текущее действие в "SetLanguage"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        StringBuilder languages = new StringBuilder();
        for (Language language : Language.all()) {
            languages.append(language.toString()).append("\n");
        }
        sendText(chatId, String.format(
                getTextByChatId(chatId, "commands/set_language/instruction"),
                languages
        ));
        User.updateCurrentAction(chatId, "SetLanguage");
    }
}
