package telegrambot.bot.controllers.commands;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.helpers.TextListExtractor.getFilesTextList;
import static telegrambot.bot.views.MessageSender.sendText;

public class AddFileCommand implements Commandable {

    private final static String COMMAND_NAME = "/add_file";

    /**
     * @param message
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        sendText(chatId, getFilesTextList(user.getLanguage()));
        sendText(chatId, getTextByUser(user, "commands/add_file/instruction"));
        User.updateCurrentAction(chatId, "AddFile");
    }

    /**
     * @return
     */
    @Override
    public boolean forAdmin() {
        return true;
    }
}
