package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для активации прав администратора
 */
public class ActivateAdmin implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    public boolean onlyText() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Дает права администратора пользователю и переводит текущее действие в "Waiting", если пользователь найден,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        String outMessageCode;
        int userId = Integer.parseInt(message.getText());
        User user = User.findById(userId);
        if (user != null && !user.isAdmin()) {
            user.setAdminRight(true);
            if (user.save()) {
                outMessageCode = "actions/activate_admin/successful";
                sendText(user.getChatId(), getTextByUser(user, "actions/activate_admin/message_to_user"));
            } else {
                outMessageCode = "general/fail/execution";
            }
        } else {
            outMessageCode = "general/fail/uncorrected ";
        }
        sendText(chatId, getTextByChatId(chatId, outMessageCode));
    }
}