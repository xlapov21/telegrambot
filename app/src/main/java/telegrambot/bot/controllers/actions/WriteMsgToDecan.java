package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.EnvVarSelector.getDecanChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.helpers.MessageResender.resendMessage;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для отправки сообщения пользователю
 */
public class WriteMsgToDecan implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    public boolean onlyText() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Перенаправляет сообщение в чат деканата и переводит текущее действие в "Waiting", если сообщение валидное,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        String outMessageCode;
        if (resendMessage(
                getDecanChatId(),
                String.format(
                        getTextByChatId(getDecanChatId(), "actions/write_msg_to_decan/title"),
                        User.findByChatId(chatId).toString()
                ),
                message,
                false
        )) {
            outMessageCode = "general/send_message/successful";
            User.updateCurrentAction(chatId, "Waiting");
        } else {
            outMessageCode = "general/send_message/unsuccessful/uncorrected";
        }
        sendText(chatId, getTextByChatId(chatId, outMessageCode));
    }
}
