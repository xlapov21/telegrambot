package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для изменения имени и фамилии пользователя в БД
 */
public class SetName implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    public boolean onlyText() {
        return true;
    }

    /**
     * Регулярное выражение для определения новых фамилии и имени
     */
    private static final String REGEX = "^([А-ЯЁ][а-яё]+)\\s+([А-ЯЁ][а-яё]+)$";

    /**
     * Паттерн для определения новых фамилии и имени
     */
    private static final Pattern PATTERN = Pattern.compile(REGEX);

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Обновляет фамилию и имя в БД и переводит текущее действие в "Waiting", если новые значения валидны,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public void doSmth(Message message) {
        Matcher matcher = PATTERN.matcher(message.getText());
        String chatId = message.getChatId().toString();
        String outMessage;
        if (matcher.find()) {
            User user = User.findByChatId(chatId);
            user.setLastName(matcher.group(1));
            user.setFirstName(matcher.group(2));
            if (user.save()) {
                outMessage = "actions/set_name/successful";
                User.updateCurrentAction(chatId, "Waiting");
            } else {
                outMessage = "general/fail/save";
            }
        } else {
            outMessage = "general/fail/uncorrected";
        }
        sendText(chatId, getTextByChatId(chatId, outMessage));
    }
}
