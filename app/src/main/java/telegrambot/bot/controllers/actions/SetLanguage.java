package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.models.Language.hasLanguage;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для смены языка пользователя в БД
 */
public class SetLanguage implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    @Override
    public boolean onlyText() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Меняет язык пользователя, и переводит текущее действие в "Waiting", если язык есть в БД,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        String languageCode = message.getText();
        String outMessageCode;
        User user = User.findByChatId(chatId);
        if (hasLanguage(languageCode)) {
            user.setLanguage(languageCode);
            if (user.save()) {
                outMessageCode = "actions/set_language/successful";
                User.updateCurrentAction(chatId, "Waiting");
            } else {
                outMessageCode = "general/fail/save";
            }
        } else {
            outMessageCode = "general/fail/uncorrected";
        }
        sendText(chatId, getTextByUser(user, outMessageCode));
    }
}
