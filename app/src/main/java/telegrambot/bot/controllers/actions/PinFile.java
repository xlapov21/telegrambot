package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.PinnedFile;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для закрепления файла
 */
public class PinFile implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    @Override
    public boolean onlyText() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Обновляет или создает новый закрепленный файл по подписи файла, переводит текущее действие в "Waiting"
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        String outMessageCode;
        if (message.hasDocument()) {
            String title = message.getCaption();
            if (title != null) {
                String fileId = message.getDocument().getFileId();
                String oldUserChatId = null;
                PinnedFile file = PinnedFile.findByTitle(title);
                if (file == null) {
                    file = new PinnedFile(-1, title, fileId, chatId);
                } else {
                    oldUserChatId = file.getUserChatId();
                    file.setFileId(fileId);
                    file.setUserChatId(chatId);
                }
                if (file.save()) {
                    outMessageCode = "actions/pin_file/successful";
                    if (!chatId.equals(oldUserChatId)) {
                        sendText(oldUserChatId, String.format(getTextByChatId(
                                oldUserChatId,
                                "actions/pin_file/message_for_old_holder"
                        ), title));
                    }
                    User.updateCurrentAction(chatId, "Waiting");
                } else {
                    outMessageCode = "general/fail/save";
                }
            } else {
                outMessageCode = "actions/pin_file/unsuccessful/hasn`t_caption";
            }
        } else {
            outMessageCode = "actions/pin_file/unsuccessful/hasn`t_document";
        }
        sendText(chatId, getTextByChatId(chatId, outMessageCode));
    }
}
