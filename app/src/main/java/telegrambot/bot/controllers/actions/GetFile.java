package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.PinnedFile;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.views.MessageSender.sendFileByFileId;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для получения закрепленного файла
 */
public class GetFile implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    @Override
    public boolean onlyText() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет пользователю файл и переводит текущее действие в "Waiting", если он найден,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        int id = Integer.parseInt(message.getText());
        PinnedFile file = PinnedFile.findById(id);
        if (file != null) {
            sendFileByFileId(chatId, file.getTitle(), file.getFileId());
            User.updateCurrentAction(chatId, "Waiting");
        } else {
            sendText(chatId, getTextByChatId(chatId, "general/fail/uncorrected"));
        }
    }
}
