package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.helpers.MessageResender;
import telegrambot.bot.models.Group;
import telegrambot.bot.models.User;
import telegrambot.bot.models.UserToGroup;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для отправки сообщения пользователям, состоящей в группе
 */
class WriteMsgToGroup {

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Перенаправляет сообщение пользователям, состоящим в группе с тем же названием, что и префикс, либо всем, если префикс == All.
     * Если группа не найдена, предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public static void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        String groupName = MessageResender.getPrefix(MessageResender.getMessageText(message));
        String outMessageCode = SendMessageToGroup(groupName, message);
        if (outMessageCode.equals("general/send_message/successful")) {
            User.updateCurrentAction(chatId, "Waiting");
        }
        sendText(chatId, getTextByChatId(chatId, outMessageCode));
    }

    /**
     * Вспомогательный метод для отправки сообщения пользователям в группе и формирования ответного кода сообщения
     *
     * @param groupName - имя группы для пересылки, либо All
     * @param message   - экземпляр класса Message, несущий информацию о чате
     * @return код ответного сообщения
     */
    private static String SendMessageToGroup(String groupName, Message message) {
        String outMessageCode = "general/send_message/successful";
        if (groupName.equals("All")) {
            for (User user : User.findRegistered()) {
                if (!MessageResender.resendMessage(
                        user.getChatId(),
                        getTextByUser(user, "actions/write_msg_to_group/title/all"),
                        message,
                        true
                )) {
                    outMessageCode = "general/send_message/unsuccessful/uncorrected";
                    break;
                }
            }
        } else if (Group.hasGroup(groupName)) {
            for (User user : UserToGroup.findUsersByGroup(groupName)) {
                if (!MessageResender.resendMessage(
                        user.getChatId(),
                        String.format(getTextByUser(user, "actions/write_msg_to_group/title/general"), groupName),
                        message,
                        true
                )) {
                    outMessageCode = "general/send_message/unsuccessful/uncorrected";
                    break;
                }
            }
        } else {
            outMessageCode = "general/send_message/unsuccessful/not_found_group";
        }
        return outMessageCode;
    }
}
