package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.PinnedFile;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для удаления файла из закрепленных
 */
public class DeleteFile implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    @Override
    public boolean onlyText() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Удаляет файл из закрепленных и переводит текущее действие в "Waiting", если он найден,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        int id = Integer.parseInt(message.getText());
        String outMessageCode;
        PinnedFile file = PinnedFile.findById(id);
        if (file != null) {
            if (file.destroy()) {
                String oldUserChatId = file.getUserChatId();
                if (!chatId.equals(oldUserChatId)) {
                    sendText(oldUserChatId, String.format(getTextByChatId(
                            oldUserChatId,
                            "actions/delete_file/message_for_old_holder"
                    ), file.getTitle()));
                }
                User.updateCurrentAction(chatId, "Waiting");
                outMessageCode = "actions/delete_file/successful";
            } else {
                outMessageCode = "general/fail/execution";
            }
        } else {
            outMessageCode = "general/fail/uncorrected";
        }
        sendText(chatId, getTextByChatId(chatId, outMessageCode));
    }
}
