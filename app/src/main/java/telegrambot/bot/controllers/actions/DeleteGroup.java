package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.Group;
import telegrambot.bot.models.User;
import telegrambot.bot.models.UserToGroup;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для удаления группы
 */
public class DeleteGroup implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    @Override
    public boolean onlyText() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Удаляет группу, предупреждает участников группы о ее удалении и переводит текущее действие в "Waiting", если группа найдена,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        String outMessageCode;
        String groupName = message.getText();
        if (Group.hasGroup(groupName)) {
            User[] users = UserToGroup.findUsersByGroup(groupName);
            if (Group.findByName(groupName).destroy()) {
                String warningMessageCode = "actions/delete_group/warning";
                for (User user : users) {
                    sendText(user.getChatId(), String.format(getTextByUser(user, warningMessageCode), groupName));
                }
                outMessageCode = "actions/delete_group/successful";
                User.updateCurrentAction(chatId, "Waiting");
            } else {
                outMessageCode = "general/fail/execution";
            }
        } else {
            outMessageCode = "general/send_message/unsuccessful/not_found_group";
        }
        sendText(chatId, getTextByChatId(chatId, outMessageCode));
    }
}
