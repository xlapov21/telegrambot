package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.User;
import telegrambot.bot.models.UserToGroup;

import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для выхода из группы
 */
public class ExitFromGroup implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    @Override
    public boolean onlyText() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Удаляет связь пользователя между ним и группой и переводит текущее действие в "Waiting", если он состоял в группе,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        int userId = user.getId();
        String outMessageCode;
        String groupName = message.getText();
        if (UserToGroup.UserIsInGroup(userId, groupName)) {
            if (UserToGroup.getUserToGroup(userId, groupName).destroy()) {
                outMessageCode = "actions/exit_from_group/successful";
                User.updateCurrentAction(chatId, "Waiting");
            } else {
                outMessageCode = "general/fail/execution";
            }
        } else {
            outMessageCode = "actions/exit_from_group/unsuccessful/not_in_a_group";
        }
        sendText(chatId, getTextByUser(user, outMessageCode));
    }
}
