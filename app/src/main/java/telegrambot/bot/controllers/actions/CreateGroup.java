package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.Group;
import telegrambot.bot.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для создания группы
 */
public class CreateGroup implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    public boolean onlyText() {
        return true;
    }

    /**
     * Регулярное выражение для определения новой группы
     */
    private static final String REGEX = "^(open|close)\\s*-\\s*(hidden|visible)\\s*-\\s*(\\S+)\\s*-\\s*\\b(.+)$";

    /**
     * Паттерн для определения новой группы
     */
    private static final Pattern PATTERN = Pattern.compile(REGEX, Pattern.UNICODE_CHARACTER_CLASS);

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Обновляет или создает новую группу, переводит текущее действие в "Waiting", текст сообщения валидный,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        Matcher matcher = PATTERN.matcher(message.getText());
        String outMessageCode;
        if (matcher.find()) {
            boolean isClosed = matcher.group(1).equals("close");
            boolean isHidden = matcher.group(1).equals("hidden");
            Group group = Group.findByName(matcher.group(3));
            if (group == null) {
                outMessageCode = (Group.create(
                        matcher.group(3),
                        matcher.group(4),
                        isClosed,
                        isHidden
                )) ? "actions/create_group/successful" : "general/fail/save";
            } else {
                group.setTitle(matcher.group(4));
                group.setIsClosed(isClosed);
                group.setIsHidden(isHidden);
                outMessageCode = (group.save()) ? "actions/create_group/successful" : "general/fail/save";
            }
        } else {
            outMessageCode = "general/fail/uncorrected";
        }
        if (outMessageCode.equals("actions/create_group/successful")) {
            User.updateCurrentAction(chatId, "Waiting");
        }
        sendText(chatId, getTextByChatId(chatId, outMessageCode));
    }
}
