package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.helpers.MessageResender;
import telegrambot.bot.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.helpers.MessageResender.resendMessage;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для отправки сообщения пользователю
 */
public class WriteMsgToUser implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    public boolean onlyText() {
        return false;
    }

    /**
     * Регулярное выражение для отделения префикса- ID пользователя и текста для отправки
     */
    private static final String REGEX = "^(\\d+)\\s(.+)$";

    /**
     * Паттерн для отделения префикса- ID пользователя и текста для отправки
     */
    private static final Pattern PATTERN = Pattern.compile(REGEX);

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Если в качестве префикса дано не число, автоматически перебрасывает в действие WriteMsgToGroup.
     * Перенаправляет сообщение пользователю и переводит текущее действие в "Waiting", если он найден,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        Matcher matcher = PATTERN.matcher(MessageResender.getMessageText(message));
        if (matcher.find()) {
            String outMessageCode;
            User user = User.findById(Integer.parseInt(matcher.group(1)));
            if (user != null) {
                resendMessage(user.getChatId(), getTextByUser(user, "actions/write_msg_to_user/title"), message, true);
                outMessageCode = "general/send_message/successful";
                User.updateCurrentAction(chatId, "Waiting");
            } else {
                outMessageCode = "general/send_message/unsuccessful/not_found_user";
            }
            sendText(chatId, getTextByChatId(chatId, outMessageCode));

        } else {
            WriteMsgToGroup.doSmth(message);
        }
    }

}
