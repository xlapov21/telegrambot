package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.Group;
import telegrambot.bot.models.User;
import telegrambot.bot.models.UserToGroup;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие для добавления пользователя в группу
 */
public class AddUserToGroup implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    @Override
    public boolean onlyText() {
        return true;
    }

    /**
     * Регулярное выражение для отделения ID пользователя и текста для отправки
     */
    private static final String REGEX = "^(\\d+)\\s*(\\S+)$";

    /**
     * Паттерн для отделения ID пользователя и текста для отправки
     */
    private static final Pattern PATTERN = Pattern.compile(REGEX, Pattern.UNICODE_CHARACTER_CLASS);

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Создает связь группа-пользователь и переводит текущее действие в "Waiting", если группа и пользователь найдены,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        Matcher matcher = PATTERN.matcher(message.getText());
        String outMessageCode;
        if (matcher.find()) {
            int userId = Integer.parseInt(matcher.group(1));
            String groupName = matcher.group(2);
            User user = User.findById(userId);
            if (user != null){
                Group group = Group.findByName(groupName);
                if (group != null){
                    if (!UserToGroup.UserIsInGroup(userId, groupName)){
                        if (UserToGroup.create(userId, groupName)){
                            outMessageCode = "actions/addition_user_to_group/successful";
                            sendText(user.getChatId(), String.format(
                                    getTextByUser(user, "actions/addition_user_to_group/message_to_user"),
                                    group.toSimpleString()
                            ));
                        } else {
                            outMessageCode = "general/fail/execution";
                        }
                    } else {
                        outMessageCode = "actions/addition_user_to_group/unsuccessful/already_been";
                    }
                    User.updateCurrentAction(chatId, "Waiting");
                } else {
                    outMessageCode = "general/send_message/unsuccessful/not_found_group";
                }
            } else {
                outMessageCode = "general/send_message/unsuccessful/not_found_user";
            }
        } else {
            outMessageCode = "general/fail/uncorrected";
        }
        sendText(chatId, getTextByChatId(chatId, outMessageCode));
    }
}
