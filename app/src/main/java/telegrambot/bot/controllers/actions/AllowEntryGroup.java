package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.Group;
import telegrambot.bot.models.User;
import telegrambot.bot.models.UserToGroup;

import static telegrambot.bot.lib.EnvVarSelector.getDecanChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие входа в группу
 */
public class AllowEntryGroup implements Actionable {

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    @Override
    public boolean onlyText() {
        return true;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Добавляет пользователя в группу или отправляет сообщение о желании войти деканату,
     * если группа приватная и пользователь не является администратором,
     * и переводит текущее действие в "Waiting", если группа найдена,
     * иначе предупреждает о некорректности данных.
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    @Override
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        String groupName = message.getText();
        String outMessageCode;
        if (Group.hasGroup(groupName)) {
            if (!UserToGroup.UserIsInGroup(user.getId(), groupName)) {
                Group group = Group.findByName(groupName);
                if (group.isClosed()) {
                    if (user.isAdmin()) {
                        outMessageCode = getCodeByCreate(user, groupName);
                    } else {
                        String decanChatId = getDecanChatId();
                        sendText(decanChatId, String.format(
                                getTextByChatId(decanChatId, "actions/allow_entry_group/message_to_decan"),
                                user,
                                group.toSimpleString()
                        ));
                        outMessageCode = "actions/allow_entry_group/unsuccessful/haven`t_low";
                        User.updateCurrentAction(user.getChatId(), "Waiting");
                    }
                } else {
                    outMessageCode = getCodeByCreate(user, groupName);
                }
            } else {
                outMessageCode = "actions/allow_entry_group/unsuccessful/already_in_group";
                User.updateCurrentAction(user.getChatId(), "Waiting");
            }
        } else {
            outMessageCode = "general/send_message/unsuccessful/not_found_group";
        }
        sendText(chatId, getTextByUser(user, outMessageCode));
    }

    /**
     * Вспомогательный метод, пробует создать связь группа-пользователь и дает код ответного сообщения об успешности
     * или не успешности создания с переводом текущего действия пользователя в "Waiting"
     *
     * @param user      - пользователь
     * @param groupName - имя группы
     * @return код ответного сообщения
     */
    private static String getCodeByCreate(User user, String groupName) {
        if (UserToGroup.create(user.getId(), groupName)) {
            User.updateCurrentAction(user.getChatId(), "Waiting");
            return "actions/allow_entry_group/successful";
        } else {
            return "general/fail/execution";
        }
    }
}
