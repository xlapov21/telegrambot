package telegrambot.bot.controllers.actions;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.models.User;

import static telegrambot.bot.lib.EnvVarSelector.getEnvVar;
import static telegrambot.bot.lib.MessageSelector.getTextByChatId;
import static telegrambot.bot.lib.MessageSelector.getTextByUser;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс контроллер-действие, говорящей об ожидании команды
 */
public class Waiting implements Actionable {

    /**
     * Количество вариантов actions/waiting/options/*
     */
    private static final int OPTIONS_NUMBER = 7;

    /**
     * Метод определения ожидаемого сообщения при этом действии
     *
     * @return булевое значение, ожидается ли только текстовое сообщение, или возможны вложения
     */
    public boolean onlyText() {
        return false;
    }

    /**
     * Метод основного действия. Точка входа в контроллер.
     * Отправляет рандомное сообщение из actions/waiting/options/* или активирует права администратора, если отправлен ключ администратора
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public void doSmth(Message message) {
        String chatId = message.getChatId().toString();
        if (message.hasText() && message.getText().equals(getEnvVar("ADMIN_KEY"))) {
            User user = User.findByChatId(chatId);
            user.setAdminRight(true);
            if (user.save()){
                sendText(chatId, getTextByUser(user, "actions/activate_admin/message_to_user"));
            }
        } else {
            int option_num = (int) (Math.random() * OPTIONS_NUMBER);
            sendText(chatId, getTextByChatId(chatId, "actions/waiting/options/" + option_num));
        }
    }
}
