package telegrambot.bot.lib;

import io.github.cdimascio.dotenv.Dotenv;

/**
 * Класс - выниматель переменных среды
 *
 * @author Хлапов Егор
 */
public class EnvVarSelector {

    /**
     * Метод - выниматель переменных среды из файла .env
     *
     * @param nameVar название переменной среды
     */
    public static String getEnvVar(String nameVar) {
        return Dotenv.load().get(nameVar);
    }

    /**
     * Метод - выниматель id чата деканата из файла .env
     */
    public static String getDecanChatId(){
        return getEnvVar("DECAN_CHAT_ID");
    }
}
