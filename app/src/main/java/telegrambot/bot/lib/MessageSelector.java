package telegrambot.bot.lib;


import telegrambot.bot.models.User;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Класс - выниматель теста сообщений из MessagesBundle по коду сообщений
 */
public class MessageSelector {

    /**
     * Метод - выниматель текста сообщения по языку локализации и коду сообщения
     *
     * @param language    - язык локализации
     * @param messageCode - код сообщения
     * @return текст сообщения
     */
    public static String getTextByLanguage(String language, String messageCode) {
        return ResourceBundle.getBundle("locale/MessagesBundle", new Locale(language)).getString(messageCode);
    }

    /**
     * Метод - выниматель текста сообщения по языку локализации пользователя и коду сообщения
     *
     * @param user        - пользователь
     * @param messageCode - код сообщения
     * @return текст сообщения
     */
    public static String getTextByUser(User user, String messageCode) {
        return getTextByLanguage((user == null) ? "ru" : user.getLanguage(), messageCode);
    }

    /**
     * Метод - выниматель текста сообщения по языку локализации пользователя с определенным ID чата и коду сообщения
     *
     * @param chatId      - ID чата пользователя
     * @param messageCode - код сообщения
     * @return текст сообщения
     */
    public static String getTextByChatId(String chatId, String messageCode) {
        return getTextByUser(User.findByChatId(chatId), messageCode);
    }
}
