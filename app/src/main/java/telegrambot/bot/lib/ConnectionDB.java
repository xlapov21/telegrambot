package telegrambot.bot.lib;

import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

import static telegrambot.bot.lib.EnvVarSelector.getEnvVar;


/**
 * Класс для связи моделей с БД
 *
 * @author Хлапов Егор
 */
public class ConnectionDB<T> {

    /**
     * интерфейс для лямбда-функций, применяемых в executeSelect(String, ModelExtractor<T>)
     */
    public interface ModelExtractor<E> {
        E newModel(ResultSet rs);
    }

    /**
     * экземпляр Connection
     */
    private static Connection connection;

    /**
     * метод коннекта к БД
     */
    private static void createConn() {
        Properties connectionProps = new Properties();
        connectionProps.put("user", getEnvVar("DB_CONNECTION_USER"));
        connectionProps.put("password", getEnvVar("DB_CONNECTION_PASSWORD"));
        try {
            connection = DriverManager.getConnection(getEnvVar("DB_CONNECTION_URL"), connectionProps);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * геттер conn
     *
     * @return возвращает синглтон экземпляра Connection
     */
    public static Connection getConn() {
        if (connection == null) {
            createConn();
        }
        return connection;
    }

    /**
     * геттер экземпляра PreparedStatement
     *
     * @param query - SQL запрос
     * @return возвращает экземпляр PreparedStatement
     */
    public static PreparedStatement getPreparedStatement(String query) {
        PreparedStatement stmt = null;
        try {
            stmt = getConn().prepareStatement(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;
    }

    /**
     * геттер экземпляра PreparedStatement
     *
     * @param query            - SQL запрос
     * @param generatedColumns - массив колонок, сгенерированные значения которых можно посмотреть в ResultSet
     * @return возвращает экземпляр PreparedStatement
     */
    public static PreparedStatement getPreparedStatement(String query, String[] generatedColumns) {
        PreparedStatement stmt = null;
        try {
            stmt = getConn().prepareStatement(query, generatedColumns);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;
    }

    /**
     * метод выполнения запроса в БД, не возвращающего экземпляр ResultSet
     *
     * @param query - SQL запрос
     */
    public static void executeWithOutRs(String query) {
        try (Statement stmt = getConn().createStatement()) {
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * метод получения экземпляров T по SQL запросу к БД
     *
     * @param query  - SQL запрос
     * @param lambda - лямбда-функция, предназначенная для создания экземпляра T
     * @return возвращает список экземпляров T
     */
    public ArrayList<T> executeSelect(String query, ModelExtractor<T> lambda) {
        ArrayList<T> models = new ArrayList<>();
        try (Statement stmt = getConn().createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            try {
                while (rs.next()) {
                    models.add(lambda.newModel(rs));
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return models;
    }

}
