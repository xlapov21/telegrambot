package telegrambot.bot.configs;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.controllers.actions.*;
import telegrambot.bot.controllers.commands.*;
import telegrambot.bot.controllers.concerns.Actionable;
import telegrambot.bot.controllers.concerns.Commandable;
import telegrambot.bot.models.User;
import telegrambot.bot.lib.MessageSelector;

import static telegrambot.bot.helpers.FieldGetter.getCommandName;
import static telegrambot.bot.views.MessageSender.sendText;

/**
 * Класс - роутер сообщений
 */
public class Redirector {

    /**
     * Массив с экземплярами контроллеров - команд
     */
    private static final Commandable[] COMMANDS =
            {
                    new StartCommand(),
                    new HelpCommand(),
                    new RenameCommand(),
                    new WriteToDecanCommand(),
                    new WriteFromDecanCommand(),
                    new ActivateAdminCommand(),
                    new CreateGroupCommand(),
                    new DeleteGroupCommand(),
                    new AddUserToGroupCommand(),
                    new AllowEntryGroupCommand(),
                    new ExitFromGroupCommand(),
                    new SetLanguageCommand(),
                    new PinFileCommand(),
                    new GetFileCommand(),
                    new DeleteFileCommand()
            };

    /**
     * Массив с экземплярами контроллеров - действий
     */
    private static final Actionable[] ACTIONS =
            {
                    new SetName(),
                    new Waiting(),
                    new WriteMsgToDecan(),
                    new WriteMsgToUser(),
                    new CreateGroup(),
                    new DeleteGroup(),
                    new AddUserToGroup(),
                    new AllowEntryGroup(),
                    new ExitFromGroup(),
                    new SetLanguage(),
                    new GetFile(),
                    new PinFile(),
                    new DeleteFile(),
                    new ActivateAdmin()
            };

    /**
     * Геттер COMMANDS
     *
     * @return значение COMMANDS
     */
    public static Commandable[] getCommands() {
        return COMMANDS;
    }

    /**
     * Метод передачи сообщения контроллерам - командам
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public static void directCommand(Message message) {
        String textMessage = message.getText();
        String outMessageCode = null;
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        if (textMessage.equals("/start") || !(user == null || user.getFirstName().equals("null"))) {
            boolean foundCommand = false;
            for (Commandable command : COMMANDS) {
                String commandName = getCommandName(command);
                if (commandName.equals(textMessage)) {
                    if (command.forAdmin()) {
                        if (user.isAdmin()) {
                            command.doSmth(message);
                        } else {
                            User.updateCurrentAction(chatId, "Waiting");
                            outMessageCode = "general/ban/not_enough_rights";
                        }
                    } else {
                        command.doSmth(message);
                    }
                    foundCommand = true;
                    break;
                }
            }
            if (!foundCommand) {
                outMessageCode = "redirector/commands/not_found";
            }
        } else {
            outMessageCode = "general/ban/not_registered";
        }
        sendText(chatId, MessageSelector.getTextByUser(user, outMessageCode));
    }

    /**
     * Метод передачи сообщения контроллерам - действиям
     *
     * @param message - экземпляр класса Message, несущий информацию о чате
     */
    public static void directAction(Message message) {
        String chatId = message.getChatId().toString();
        User user = User.findByChatId(chatId);
        String outMessageCode = null;
        if (user == null) {
            outMessageCode = "general/ban/not_registered";
        } else {
            String currentAction = user.getCurrentAction();
            boolean foundAction = false;
            for (Actionable action : ACTIONS) {
                if (currentAction.equals(action.getTitle())) {
                    if (action.onlyText()) {
                        if (message.hasText()) {
                            action.doSmth(message);
                        } else {
                            outMessageCode = "redirector/actions/only_text";
                        }
                    } else {
                        action.doSmth(message);
                    }
                    foundAction = true;
                    break;
                }
            }
            if (!foundAction) {
                outMessageCode = "redirector/actions/not_found_action";
            }
        }
        sendText(chatId, MessageSelector.getTextByUser(user, outMessageCode));
    }
}
