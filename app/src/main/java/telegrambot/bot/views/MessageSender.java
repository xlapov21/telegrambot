package telegrambot.bot.views;

import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.stickers.Sticker;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import static telegrambot.bot.Bot.getBot;

/*
 * Класс для отправки сообщений
 */
public class MessageSender {

    /**
     * Метод для отправки текстового сообщения в чат
     *
     * @param chatId ID чата для отправки
     * @param text   текст для отправки
     */
    public static void sendText(String chatId, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(text);
        try {
            getBot().execute(sendMessage);
        } catch (TelegramApiException e) {
        }
    }

    /**
     * Метод для отправки фото с подписью в чат
     *
     * @param chatId  ID чата для отправки
     * @param caption подпись
     * @param photo   объект PhotoSize
     */
    public static void sendPhoto(String chatId, String caption, PhotoSize photo) {
        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setChatId(chatId);
        sendPhoto.setCaption(caption);
        sendPhoto.setPhoto(new InputFile(photo.getFileId()));
        try {
            getBot().execute(sendPhoto);
        } catch (TelegramApiException e) {
        }
    }

    /**
     * Метод для отправки документа с подписью в чат
     *
     * @param chatId   ID чата для отправки
     * @param caption  подпись
     * @param document объект Document
     */
    public static void sendDocument(String chatId, String caption, Document document) {
        SendDocument sendDocument = new SendDocument();
        sendDocument.setCaption(caption);
        sendDocument.setChatId(chatId);
        sendDocument.setDocument(new InputFile(document.getFileId()));

        try {
            getBot().execute(sendDocument);
        } catch (TelegramApiException e) {
        }
    }

    /**
     * Метод для отправки документа с подписью в чат
     *
     * @param chatId  ID чата для отправки
     * @param caption подпись
     * @param fileId  id файла
     */
    public static void sendFileByFileId(String chatId, String caption, String fileId) {
        SendDocument sendDocument = new SendDocument();
        sendDocument.setCaption(caption);
        sendDocument.setChatId(chatId);
        sendDocument.setDocument(new InputFile(fileId));
        try {
            getBot().execute(sendDocument);
        } catch (TelegramApiException e) {
        }
    }

    /**
     * Метод для отправки стикера с подписью в чат
     *
     * @param chatId  ID чата для отправки
     * @param caption подпись
     * @param sticker объект Sticker
     */
    public static void sendSticker(String chatId, String caption, Sticker sticker) {
        sendText(chatId, caption);
        SendSticker sendSticker = new SendSticker();
        sendSticker.setSticker(new InputFile(sticker.getFileId()));
        sendSticker.setChatId(chatId);
        try {
            getBot().execute(sendSticker);
        } catch (TelegramApiException e) {
        }
    }
}
