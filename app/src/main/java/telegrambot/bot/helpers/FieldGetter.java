package telegrambot.bot.helpers;

import telegrambot.bot.controllers.concerns.Commandable;

import java.lang.reflect.Field;

/**
 * Вспомогательный класс - выниматель значений полей из объектов
 */
public class FieldGetter {

    /**
     * Метод получения значения поля COMMAND_NAME из экземпляра Commandable
     *
     * @param command - экземпляр класса, реализующего интерфейс Commandable
     * @return значения поля COMMAND_NAME
     */
    public static String getCommandName(Commandable command) {
        return getField(command, "COMMAND_NAME");
    }

    /**
     * Метод получения значения поля из объекта
     *
     * @param object    - объект, из которого будет доставаться значения поля
     * @param fieldName - название поля
     * @return значения поля
     */
    public static String getField(Object object, String fieldName) {
        String fieldValue = null;
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            fieldValue = (String) field.get(fieldName);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldValue;
    }
}
