package telegrambot.bot.helpers;

import telegrambot.bot.models.Group;
import telegrambot.bot.models.PinnedFile;
import telegrambot.bot.models.User;
import telegrambot.bot.models.UserToGroup;

import static telegrambot.bot.lib.MessageSelector.getTextByLanguage;

/**
 * Вспомогательный класс - формирователь текстовых списков
 */
public class TextListExtractor {

    /**
     * метод генерации строкового списка всех пользователей из таблицы users
     *
     * @param language - применяемый язык
     * @return строка с перечисленными пользователями
     */
    public static String getAllUsersTextList(String language) {
        StringBuilder usersList = new StringBuilder(getTextByLanguage(language, "general/list/users") + "\n");
        for (User user : User.all()) {
            usersList.append(user.toString()).append("\n");
        }
        return usersList.toString();
    }

    /**
     * метод генерации строкового списка зарегистрированных пользователей из таблицы users
     *
     * @param language - применяемый язык
     * @return строка с перечисленными пользователями
     */
    public static String getUsersTextList(String language) {
        StringBuilder users = new StringBuilder(getTextByLanguage(language, "general/list/users") + "\n");
        for (User user : User.findRegistered()) {
            users.append(user.toString()).append("\n");
        }
        return users.toString();
    }


    /**
     * метод генерации строкового списка всех пользователей с правами администратора из таблицы users
     *
     * @param language - применяемый язык
     * @return строка с перечисленными пользователями
     */
    public static String getAdminsTextList(String language) {
        StringBuilder admins = new StringBuilder(getTextByLanguage(language, "general/list/admins") + "\n");
        for (User user : User.findAdmins()) {
            admins.append(user.toString()).append("\n");
        }
        return admins.toString();
    }


    /**
     * метод генерации строкового списка зарегистрированных пользователей без прав администратора из таблицы users
     *
     * @param language - применяемый язык
     * @return строка с перечисленными пользователями
     */
    public static String getNotAdminsTextList(String language) {
        StringBuilder users = new StringBuilder(getTextByLanguage(language, "general/list/users") + "\n");
        for (User user : User.findNotAdmins()) {
            users.append(user.toString()).append("\n");
        }
        return users.toString();
    }

    /**
     * метод генерации строкового списка всех групп из таблицы groups
     *
     * @param language - применяемый язык
     * @return строка с перечисленными группами
     */
    public static String getGroupsTextList(String language) {
        StringBuilder groups = new StringBuilder(getTextByLanguage(language, "general/list/groups") + "\n");
        for (Group group : Group.all()) {
            groups.append(group.toString()).append("\n");
        }
        return groups.toString();
    }


    /**
     * метод генерации строкового списка всех групп с основной информацией из таблицы groups
     *
     * @param language - применяемый язык
     * @return строка с перечисленными группами
     */
    public static String getGroupsSimpleTextList(String language) {
        StringBuilder groups = new StringBuilder(getTextByLanguage(language, "general/list/groups") + "\n");
        for (Group group : Group.all()) {
            groups.append(group.toSimpleString()).append("\n");
        }
        return groups.toString();
    }

    /**
     * метод генерации строкового списка всех связей группа-пользователь из таблицы users_to_group
     *
     * @param language - применяемый язык
     * @return строка с перечисленными связями
     */
    public static String getUsersAndGroupsTextList(String language) {
        return getUsersTextList(language) + getGroupsSimpleTextList(language);
    }

    /**
     * метод генерации строкового списка видимых групп из таблицы groups
     *
     * @param language - применяемый язык
     * @return строка с перечисленными группами
     */
    public static String getVisibleGroupsTextList(String language) {
        StringBuilder groups = new StringBuilder(getTextByLanguage(language, "general/list/groups") + "\n");
        for (Group group : Group.findVisible()) {
            groups.append(group.toSimpleString()).append("\n");
        }
        return groups.toString();
    }

    /**
     * метод генерации строкового списка групп из таблицы groups, в которых состоит пользователь с определенным ID
     *
     * @param language - применяемый язык
     * @param userId   - ID пользователя
     * @return строка с перечисленными группами
     */
    public static String getUserGroupsTextList(String language, int userId) {
        StringBuilder groups = new StringBuilder(getTextByLanguage(language, "general/list/groups") + "\n");
        for (Group group : UserToGroup.findUserGroups(userId)) {
            groups.append(group.toSimpleString()).append("\n");
        }
        return groups.toString();
    }

    /**
     * метод генерации строкового списка всех закрепленных файлов из таблицы pinned_files
     *
     * @param language - применяемый язык
     * @return строка с перечисленными файлами
     */
    public static String getFilesTextList(String language) {
        StringBuilder files = new StringBuilder(getTextByLanguage(language, "general/list/files") + "\n");
        for (PinnedFile file : PinnedFile.all()) {
            files.append(file.toString()).append("\n");
        }
        return files.toString();
    }
}
