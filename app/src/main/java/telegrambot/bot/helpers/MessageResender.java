package telegrambot.bot.helpers;

import org.telegram.telegrambots.meta.api.objects.Message;
import telegrambot.bot.views.MessageSender;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Вспомогательный класс - переотправитель сообщений
 */
public class MessageResender {

    /**
     * Регулярное выражения для отделения префикс от сообщения
     */
    private static final String REGEX = "^(\\S+)\\s+(.+)$";

    /**
     * Паттерн для отделения префикса от сообщения
     */
    private static final Pattern PATTERN = Pattern.compile(REGEX);

    /**
     * Метод перенаправки сообщения в определеннными чатом и титульником
     *
     * @param chatId    - ID чата для отправки
     * @param title     - титульник сообщения
     * @param message   - пренаправляемое сообщение
     * @param hasPrefix - имеет ли перенаправляемое сообщение ненужный префикс
     * @return булевое значение об успешности выполнения и валидности сообщения
     */
    public static boolean resendMessage(String chatId, String title, Message message, boolean hasPrefix) {
        String messageText = getMessageText(message);
        String textForSend = (messageText == null) ? title : String.format("%s\n%s", title, getTextForSend(messageText, hasPrefix));
        boolean messageDelivered = true;
        if (message.hasText()) {
            MessageSender.sendText(chatId, textForSend);
        } else if (message.hasPhoto()) {
            MessageSender.sendPhoto(chatId, textForSend, message.getPhoto().get(0));
        } else if (message.hasDocument()) {
            MessageSender.sendDocument(chatId, textForSend, message.getDocument());
        } else if (message.hasSticker()) {
            MessageSender.sendSticker(chatId, textForSend, message.getSticker());
        } else {
            messageDelivered = false;
        }
        return messageDelivered;
    }

    /**
     * Метод получения текста из сообщения
     *
     * @param message - сообщение
     * @return текст сообщения
     */
    public static String getMessageText(Message message) {
        return (message.hasText()) ? message.getText() : message.getCaption();
    }

    /**
     * Метод получения текста для отправляемого сообщения. Удаляет префикс, если он есть
     *
     * @param messageText - текст изначального сообщения
     * @param hasPrefix   - булевое значение о присутствии префикса
     * @return текст сообщения для отправки
     */
    public static String getTextForSend(String messageText, boolean hasPrefix) {
        if (hasPrefix) {
            Matcher matcher = PATTERN.matcher(messageText);
            messageText = (matcher.find()) ? matcher.group(2) : null;
        }
        return messageText;
    }

    /**
     * Метод получения префикса из текста
     *
     * @param messageText - текст сообщения
     * @return префикс сообщения
     */
    public static String getPrefix(String messageText) {
        Matcher matcher = PATTERN.matcher(messageText);
        return (matcher.find()) ? matcher.group(1) : null;
    }
}
