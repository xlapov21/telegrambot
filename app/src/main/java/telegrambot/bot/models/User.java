package telegrambot.bot.models;

import telegrambot.bot.lib.ConnectionDB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Класс - модель данных для таблицы users
 */
public class User {

    /**
     * экземпляр ConnectionDB
     */
    private static final ConnectionDB<User> connDB = new ConnectionDB<>();

    /**
     * лямбда-функция для создания экземпляра User
     */
    private static final ConnectionDB.ModelExtractor<User> modelExtractor = (rs) -> {
        User user = null;
        try {
            user = new User(
                    rs.getInt("id"),
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getString("user_name"),
                    rs.getString("chat_id"),
                    rs.getBoolean("admin_right"),
                    rs.getString("current_action"),
                    rs.getString("language")
            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return user;
    };

    /**
     * экземпляр PreparedStatement для обновления строк User в БД
     */
    private final PreparedStatement stmtUpdate = ConnectionDB.getPreparedStatement(
            String.format("UPDATE %s SET %s = ? WHERE id = ?", TABLE_NAME, String.join(" = ?, ", COLUMN_NAMES)));

    /**
     * имя таблицы, соответствующей классу User
     */
    private static final String TABLE_NAME = "users";

    /**
     * Массив часто используемых названий колонок, соответствующих таблице user
     */
    private static final String[] COLUMN_NAMES = new String[]{
            "first_name",
            "last_name",
            "user_name",
            "chat_id",
            "admin_right",
            "current_action",
            "language"
    };

    /**
     * Строка с именами колонок
     */
    private static final String ALL_COLUMN_NAMES_STRING = "id, " + String.join(", ", COLUMN_NAMES);

    /**
     * ID пользователя из users.id
     */
    private int id;

    /**
     * Имя пользователя из users.first_name
     */
    private String firstName;

    /**
     * Фамилия пользователя из users.last_name
     */
    private String lastName;

    /**
     * Никнейм пользователя из users.user_name
     */
    private final String userName;

    /**
     * ID чата пользователя из users.chat_id
     */
    private String chatId;

    /**
     * Права пользователя из users.admin_right
     */
    private boolean adminRight;

    /**
     * Текущее действие пользователя из users.current_action
     */
    private String currentAction;

    /**
     * Язык пользователя из users.language
     */
    private String language;

    /**
     * Геттер id
     *
     * @return значение id
     */
    public int getId() {
        return id;
    }

    /**
     * Геттер firstName
     *
     * @return значение firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Геттер lastName
     *
     * @return значение lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Геттер userName
     *
     * @return значение userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Геттер chatId
     *
     * @return значение chatId
     */
    public String getChatId() {
        return chatId;
    }

    /**
     * Геттер adminRight
     *
     * @return значение adminRight
     */
    public boolean isAdmin() {
        return adminRight;
    }

    /**
     * Геттер currentAction
     *
     * @return значение currentAction
     */
    public String getCurrentAction() {
        return currentAction;
    }

    /**
     * Геттер language
     *
     * @return значение language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Сеттер firstName
     *
     * @param firstName - новое значение firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Сеттер lastName
     *
     * @param lastName - новое значение lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Сеттер chatId
     *
     * @param chatId - новое значение chatId
     */
    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    /**
     * Сеттер adminRight
     *
     * @param adminRight - новое значение adminRight
     */
    public void setAdminRight(boolean adminRight) {
        this.adminRight = adminRight;
    }

    /**
     * Сеттер currentAction
     *
     * @param currentAction - новое значение currentAction
     */
    public void setCurrentAction(String currentAction) {
        this.currentAction = currentAction;
    }

    /**
     * Сеттер language
     *
     * @param language - новое значение language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * конструктор User
     *
     * @param id            - значение столбца id
     * @param firstName     - значение столбца first_name
     * @param lastName      - значение столбца last_name
     * @param userName      - значение столбца user_name
     * @param chatId        - значение столбца chat_id
     * @param adminRight    - значение столбца admin_right
     * @param currentAction - значение столбца current_action
     * @param language      - значение столбца language
     */
    public User(
            int id,
            String firstName,
            String lastName,
            String userName,
            String chatId,
            boolean adminRight,
            String currentAction,
            String language
    ) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.chatId = chatId;
        this.adminRight = adminRight;
        this.currentAction = currentAction;
        this.language = language;
    }

    /**
     * метод добавления в БД новой строки
     *
     * @param firstName     - значение столбца first_name
     * @param lastName      - значение столбца last_name
     * @param userName      - значение столбца user_name
     * @param chatId        - значение столбца chat_id
     * @param adminRight    - значение столбца admin_right
     * @param currentAction - значение столбца current_action
     * @param language      - значение столбца language
     * @return сгенерированный id или -1, если строка не создалась
     */
    public static int create(
            String firstName,
            String lastName,
            String userName,
            String chatId,
            boolean adminRight,
            String currentAction,
            String language
    ) {
        PreparedStatement stmtCreate = ConnectionDB.getPreparedStatement(
                String.format("INSERT INTO %s (%s) VALUES(?, ?, ?, ?, ?, ?, ?)", TABLE_NAME, String.join(", ", COLUMN_NAMES)),
                new String[]{"id"}
        );
        int newId = -1;
        try {
            setValues(
                    stmtCreate,
                    firstName,
                    lastName,
                    userName,
                    chatId,
                    adminRight,
                    currentAction,
                    language
            );
            stmtCreate.executeUpdate();
            ResultSet gk = stmtCreate.getGeneratedKeys();
            if (gk.next()) {
                newId = gk.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return newId;
    }

    /**
     * метод сохранения экземпляра User в БД
     *
     * @return булево значение об успешности выполнения
     */
    public boolean save() {
        if (id > 0) {
            try {
                stmtUpdate.clearParameters();
                setValues(
                        stmtUpdate,
                        firstName,
                        lastName,
                        userName,
                        chatId,
                        adminRight,
                        currentAction,
                        language
                );
                stmtUpdate.setInt(8, id);
                stmtUpdate.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            User user = findById(id);
            return user != null &&
                    user.getId() == id &&
                    user.getFirstName().equals(firstName) &&
                    user.getLastName().equals(lastName) &&
                    user.getUserName().equals(userName) &&
                    user.getChatId().equals(chatId) &&
                    user.getLanguage().equals(language) &&
                    user.isAdmin() == adminRight &&
                    user.getCurrentAction().equals(currentAction);
        } else {
            id = create(
                    firstName,
                    lastName,
                    userName,
                    chatId,
                    adminRight,
                    currentAction,
                    language
            );
            return id > 0;
        }
    }

    /**
     * Вспомогательный метод для установки значений в экземпляре PreparedStatement
     *
     * @param stmt          - экземпляр PreparedStatement
     * @param firstName     - значение столбца first_name
     * @param lastName      - значение столбца last_name
     * @param userName      - значение столбца user_name
     * @param chatId        - значение столбца chat_id
     * @param adminRight    - значение столбца admin_right
     * @param currentAction - значение столбца current_action
     * @param language      - значение столбца language
     */
    private static void setValues(
            PreparedStatement stmt,
            String firstName,
            String lastName,
            String userName,
            String chatId,
            boolean adminRight,
            String currentAction,
            String language
    ) throws SQLException {
        stmt.setString(1, firstName);
        stmt.setString(2, lastName);
        stmt.setString(3, userName);
        stmt.setString(4, chatId);
        stmt.setBoolean(5, adminRight);
        stmt.setString(6, currentAction);
        stmt.setString(7, language);
    }

    /**
     * метод удаления экземпляра User из БД
     *
     * @return булево значение об успешности выполнения
     */
    public boolean destroy() {
        ConnectionDB.executeWithOutRs(String.format("DELETE FROM %s WHERE id = %s", TABLE_NAME, id));
        return findById(id) == null;
    }

    /**
     * метод установки нового значения current_action для пользователя с определенным chat_id
     *
     * @param chatId        - значение столбца chat_id
     * @param currentAction - значение столбца current_action
     * @return булево значение об успешности выполнения
     */
    public static boolean updateCurrentAction(String chatId, String currentAction) {
        User user = findByChatId(chatId);
        user.setCurrentAction(currentAction);
        return user.save();
    }

    /**
     * Метод получения всех пользователей
     *
     * @return массив экземпляров User
     */
    public static User[] all() {
        return executeSelect("true");
    }

    /**
     * Метод получения зарегистрированных пользователей
     *
     * @return массив экземпляров User, где first_name != null
     */
    public static User[] findRegistered() {
        return executeSelect("first_name IS NOT NULL");
    }

    /**
     * Метод получения пользователей с правами администратора
     *
     * @return массив экземпляров User, где admin_right == true
     */
    public static User[] findAdmins() {
        return executeSelect("admin_right = true");
    }

    /**
     * Метод получения зарегистрированных пользователей с без прав администратора
     *
     * @return массив экземпляров User, где admin_right == false и first_name != null
     */
    public static User[] findNotAdmins() {
        return executeSelect("first_name IS NOT NULL AND admin_right = false");
    }

    /**
     * Метод получения пользователя по ID чата
     *
     * @param chatId - значение столбца chat_id
     * @return экземпляр User, где chat_id == chatId
     */
    public static User findByChatId(String chatId) {
        return executeSelectOneInstance(String.format("chat_id = '%s'", chatId));
    }

    /**
     * Метод получения пользователя по id
     *
     * @param id - значение столбца id
     * @return экземпляр User, где id == id (аргумент)
     */
    public static User findById(int id) {
        return executeSelectOneInstance("id = " + id);
    }

    /**
     * метод получения одного экземпляра User по SQL-запросу
     *
     * @return возвращает экземпляр User или null, если по запросу ничего не найдено
     */
    private static User executeSelectOneInstance(String condition) {
        User[] userInList = executeSelect(condition);
        return (userInList.length > 0) ? userInList[0] : null;
    }

    /**
     * метод получения массива экземпляров User по SQL-запросу
     *
     * @return возвращает массив экземпляров User
     */
    private static User[] executeSelect(String condition) {
        ArrayList<User> users = connDB.executeSelect(generateSelectQuery(condition), modelExtractor);
        return users.toArray(new User[0]);
    }

    /**
     * метод генерации SQL-запроса для выборки из таблицы
     *
     * @param condition - условие поиска в таблице
     * @return строка с SQL-запросом в БД
     */
    private static String generateSelectQuery(String condition) {
        return String.format("SELECT %s FROM %s WHERE %s ORDER BY id", ALL_COLUMN_NAMES_STRING, TABLE_NAME, condition);
    }

    /**
     * метод получения основной информации об экземпляре User в строковом виде
     *
     * @return возвращает строку с информацией об экземпляре User
     */
    @Override
    public String toString() {
        return String.format("%s. %s %s - @%s", id, lastName, firstName, userName);
    }

}
