package telegrambot.bot.models;

import telegrambot.bot.lib.ConnectionDB;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import static telegrambot.bot.lib.ConnectionDB.executeWithOutRs;

/**
 * Класс - модель данных для таблицы groups
 */
public class Group {

    /**
     * экземпляр ConnectionDB
     */
    private static final ConnectionDB<Group> connDB = new ConnectionDB<>();

    private final PreparedStatement stmtUpdate = ConnectionDB.getPreparedStatement(
            String.format("UPDATE %s SET %s = ? WHERE name = ?", TABLE_NAME, String.join(" = ?, ", COLUMN_NAMES)));


    /**
     * лямбда-функция для создания экземпляра Group
     */
    private static final ConnectionDB.ModelExtractor<Group> modelExtractor = (rs) -> {
        Group group = null;
        try {
            group = new Group(
                    rs.getString("name"),
                    rs.getString("title"),
                    rs.getBoolean("is_closed"),
                    rs.getBoolean("is_hidden")

            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return group;
    };

    /**
     * имя таблицы, соответствующей классу Group
     */
    private static final String TABLE_NAME = "groups";

    /**
     * Массив часто используемых названий колонок, соответствующих таблице user
     */
    private static final String[] COLUMN_NAMES = new String[]{
            "title",
            "is_closed",
            "is_hidden"
    };

    /**
     * Строка с именами колонок
     */
    private static final String COLUMN_NAMES_STRING = "name, " + String.join(", ", COLUMN_NAMES);

    /**
     * Уникальное имя - адрес группы из groups.name
     */
    private final String name;

    /**
     * Название группы из groups.title
     */
    private String title;

    /**
     * Буллевое значение о приватности группы (нужно ли разрешение
     * для входа) из groups.is_closed
     */
    private boolean isClosed;

    /**
     * Буллевое значение о видимости группы для обычных пользователей
     * из groups.is_hidden
     */
    private boolean isHidden;


    /**
     * Геттер name
     *
     * @return значение name
     */
    public String getName() {
        return name;
    }

    /**
     * Геттер title
     *
     * @return значение title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Геттер isClosed
     *
     * @return значение isClosed
     */
    public boolean isClosed() {
        return isClosed;
    }

    /**
     * Геттер isHidden
     *
     * @return значение isHidden
     */
    public boolean isHidden() {
        return isHidden;
    }

    /**
     * Сеттер title
     *
     * @param title - новое значение title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Сеттер isClosed
     *
     * @param isClosed - новое значение isClosed
     */
    public void setIsClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    /**
     * Сеттер isHidden
     *
     * @param isHidden - новое значение isHidden
     */
    public void setIsHidden(boolean isHidden) {
        this.isHidden = isHidden;
    }

    /**
     * конструктор Group
     *
     * @param name     - значение столбца name
     * @param title    - значение столбца title
     * @param isClosed - значение столбца is_closed
     * @param isHidden - значение столбца is_hidden
     */
    private Group(String name, String title, boolean isClosed, boolean isHidden) {
        this.name = name;
        this.title = title;
        this.isClosed = isClosed;
        this.isHidden = isHidden;
    }

    /**
     * метод сохранения экземпляра Group в БД
     *
     * @return булево значение об успешности выполнения
     */
    public boolean save() {
        try {
            stmtUpdate.clearParameters();
            stmtUpdate.setString(1, title);
            stmtUpdate.setBoolean(2, isClosed);
            stmtUpdate.setBoolean(3, isHidden);
            stmtUpdate.setString(4, name);
            stmtUpdate.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        Group group = findByName(name);
        return group != null &&
                group.isClosed() == isClosed &&
                group.isHidden() == isHidden &&
                group.getTitle().equals(title);
    }

    /**
     * метод добавления в БД новой строки
     *
     * @param name     - значение столбца name
     * @param title    - значение столбца title
     * @param isClosed - значение столбца is_closed
     * @param isHidden - значение столбца is_hidden
     * @return булево значение об успешности выполнения
     */
    public static boolean create(String name, String title, boolean isClosed, boolean isHidden) {
        executeWithOutRs(String.format("INSERT INTO %s (%s) VALUES('%s', '%s', %s, %s)", TABLE_NAME, COLUMN_NAMES_STRING, name, title, isClosed, isHidden));
        Group group = findByName(name);
        return group != null && group.getTitle().equals(title);
    }

    /**
     * метод удаления экземпляра Group из БД
     *
     * @return булево значение об успешности выполнения
     */
    public boolean destroy() {
        ConnectionDB.executeWithOutRs(String.format("DELETE FROM %s WHERE name = '%s'", TABLE_NAME, name));
        return findByName(name) == null;
    }

    /**
     * Метод получения всех групп
     *
     * @return массив экземпляров Group
     */
    public static Group[] all() {
        return executeSelect("true");
    }

    /**
     * Метод получения видимых групп
     *
     * @return массив экземпляров Group, где is_hidden == false
     */
    public static Group[] findVisible() {
        return executeSelect("is_hidden = false");
    }

    /**
     * Метод получения группы по уникальному адресу
     *
     * @param name - значение столбца name
     * @return экземпляр Group, где name == name (аргумент)
     */
    public static Group findByName(String name) {
        return executeSelectOneInstance(String.format("name = '%s'", name));
    }

    /**
     * Метод проверки наличия группы по уникальному адресу
     *
     * @param name - значение столбца name
     * @return булево значение о существовании группы
     */
    public static boolean hasGroup(String name) {
        return findByName(name) != null;
    }

    /**
     * метод получения одного экземпляра Group по SQL-запросу
     *
     * @return возвращает экземпляр Group или null, если по запросу ничего не найдено
     */
    private static Group executeSelectOneInstance(String condition) {
        Group[] groupInList = executeSelect(condition);
        return (groupInList.length > 0) ? groupInList[0] : null;
    }

    /**
     * метод получения массива экземпляров Group по SQL-запросу
     *
     * @return возвращает массив экземпляров Group
     */
    private static Group[] executeSelect(String condition) {
        ArrayList<Group> groups = connDB.executeSelect(generateSelectQuery(condition), modelExtractor);
        return groups.toArray(new Group[0]);
    }

    /**
     * метод генерации SQL-запроса для выборки из таблицы
     *
     * @param condition условие поиска в таблице
     * @return строка с SQL-запросом в БД
     */
    private static String generateSelectQuery(String condition) {
        return String.format("SELECT %s FROM %s WHERE %s", COLUMN_NAMES_STRING, TABLE_NAME, condition);
    }

    /**
     * метод получения всей информации об экземпляре Group в строковом виде
     *
     * @return возвращает строку с информацией об экземпляре Group
     */
    @Override
    public String toString() {
        return String.format(
                "%s - %s.   %s, %s",
                name,
                title,
                (isClosed) ? "Closed" : "Opened",
                (isHidden) ? "hidden" : "visible"
        );
    }

    /**
     * метод получения основной информации об экземпляре Group в строковом виде
     *
     * @return возвращает строку с информацией об экземпляре Group
     */
    public String toSimpleString() {
        return String.format("%s - %s", name, title);
    }

}
