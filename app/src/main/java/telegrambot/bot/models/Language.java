package telegrambot.bot.models;

import telegrambot.bot.lib.ConnectionDB;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Класс - модель данных для таблицы languages
 */
public class Language {

    /**
     * Экземпляр ConnectionDB
     */
    private static final ConnectionDB<Language> connDB = new ConnectionDB<>();

    /**
     * Лямбда-функция для создания экземпляра Language
     */
    private static final ConnectionDB.ModelExtractor<Language> modelExtractor = (rs) -> {
        Language language = null;
        try {
            language = new Language(
                    rs.getInt("id"),
                    rs.getString("code"),
                    rs.getString("title")
            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return language;
    };

    /**
     * Имя таблицы, соответствующей классу Language
     */
    private static final String TABLE_NAME = "languages";

    /**
     * Массив часто используемых названий колонок, соответствующих таблице languages
     */
    private static final String[] COLUMN_NAMES = new String[]{
            "code",
            "title"
    };

    /**
     * Строка с именами колонок
     */
    private static final String ALL_COLUMN_NAMES_STRING = "id, " + String.join(", ", COLUMN_NAMES);


    /**
     * ID языка из languages.code
     */
    private final int id;

    /**
     * Код языка из languages.code
     */
    private final String code;

    /**
     * Название языка из languages.title
     */
    private final String title;

    /**
     * Геттер code
     *
     * @return значение code
     */
    public int getId() {
        return id;
    }

    /**
     * Геттер code
     *
     * @return значение code
     */
    public String getCode() {
        return code;
    }

    /**
     * Геттер code
     *
     * @return значение code
     */
    public String getTitle() {
        return title;
    }

    /**
     * Конструктор Language
     *
     * @param code значение code
     */
    private Language(int id, String code, String title) {
        this.id = id;
        this.code = code;
        this.title = title;
    }

    /**
     * метод получения массива из всех экземпляров Language в таблице
     *
     * @return массив экземпляров Language
     */
    public static Language[] all() {
        return executeSelect("true");
    }

    /**
     * метод определения, есть ли язык с таким кодом
     *
     * @param code код языка
     * @return булевое значение, есть ли язык с таким кодом в БД
     */
    public static boolean hasLanguage(String code){
        for (Language language : Language.all()) {
            if (language.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }

    /**
     * метод получения массива экземпляров Language по условию поиска в таблице
     *
     * @param condition условие поиска в таблице
     * @return массив экземпляров Language
     */
    private static Language[] executeSelect(String condition) {
        ArrayList<Language> languages = connDB.executeSelect(generateSelectQuery(condition), modelExtractor);
        return languages.toArray(new Language[0]);
    }

    /**
     * метод генерации SQL-запроса для выборки из таблицы
     *
     * @param condition условие поиска в таблице
     * @return строка с SQL-запросом в БД
     */
    private static String generateSelectQuery(String condition) {
        return String.format("SELECT %s FROM %s WHERE %s", ALL_COLUMN_NAMES_STRING, TABLE_NAME, condition);
    }

    /**
     * метод получения основной информации об экземпляре User в строковом виде
     *
     * @return возвращает строку с информацией об экземпляре User
     */
    @Override
    public String toString() {
        return String.format("%s - %s", code, title);
    }
}
