package telegrambot.bot.models;

import telegrambot.bot.lib.ConnectionDB;

import java.sql.SQLException;
import java.util.ArrayList;

import static telegrambot.bot.lib.ConnectionDB.executeWithOutRs;

/**
 * Класс - модель данных для таблицы user_to_group
 */
public class UserToGroup {

    /**
     * Экземпляр ConnectionDB
     */
    private static final ConnectionDB<UserToGroup> connDB = new ConnectionDB<>();

    /**
     * Лямбда-функция для создания экземпляра UserToGroup
     */
    private static final ConnectionDB.ModelExtractor<UserToGroup> modelExtractor = (rs) -> {
        UserToGroup userToGroup = null;
        try {
            userToGroup = new UserToGroup(
                    rs.getInt("user_id"),
                    rs.getString("group_name")
            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return userToGroup;
    };

    /**
     * Имя таблицы, соответствующей классу UserToGroup
     */
    private static final String TABLE_NAME = "user_to_group";

    /**
     * Массив названий колонок, соответствующих таблице user_to_group
     */
    private static final String[] COLUMN_NAMES = new String[]{
            "user_id",
            "group_name",
    };

    /**
     * Строка с часто используемыми именами колонок
     */
    private static final String COLUMN_NAMES_STRING = String.join(", ", COLUMN_NAMES);

    /**
     * ID пользователя из users.id
     */
    private final int userId;

    /**
     * Имя группы из groups.group_name
     */
    private final String groupName;

    /**
     * Геттер userId
     *
     * @return - значение userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Геттер groupName
     *
     * @return - значение groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Конструктор UserToGroup
     *
     * @param userId    - значение userId
     * @param groupName - значение groupName
     */
    private UserToGroup(int userId, String groupName) {
        this.userId = userId;
        this.groupName = groupName;
    }

    /**
     * Метод получения экземпляра UserToGroup по значению userId и groupName
     *
     * @param userId    - значение userId
     * @param groupName - значение groupName
     * @return экземпляр UserToGroup, если найден в таблице, иначе null
     */
    public static UserToGroup getUserToGroup(int userId, String groupName) {
        return executeSelectOneInstance(String.format("user_id = %s and group_name = '%s'", userId, groupName));
    }

    /**
     * Метод получения экземпляров UserToGroup по значению userId
     *
     * @param userId - значение userId
     * @return массив экземпляров UserToGroup, где - значение user_id = userId
     */
    public static UserToGroup[] findUserToGroupsByUserId(int userId) {
        return executeSelect("user_id = " + userId);
    }

    /**
     * Метод получения экземпляров UserToGroup по значению groupName
     *
     * @param groupName - значение groupName
     * @return массив экземпляров UserToGroup, где - значение group_name = groupName
     */
    public static UserToGroup[] findUserToGroupsByGroupName(String groupName) {
        return executeSelect(String.format("group_name = '%s'", groupName));
    }

    /**
     * Метод получения экземпляров User по значению groupName
     *
     * @param groupName - значение group_name
     * @return массив экземпляров User, которые входят в группу с group_name = groupName
     */
    public static User[] findUsersByGroup(String groupName) {
        ArrayList<User> users = new ArrayList<>();
        for (UserToGroup connection : findUserToGroupsByGroupName(groupName)) {
            users.add(User.findById(connection.getUserId()));
        }
        return users.toArray(new User[0]);
    }

    /**
     * Метод получения экземпляров Group по значению userId
     *
     * @param userId - значение user_id
     * @return массив экземпляров Group, в которые входит пользователь с user_id = userId
     */
    public static Group[] findUserGroups(int userId) {
        ArrayList<Group> groups = new ArrayList<>();
        for (UserToGroup connection : findUserToGroupsByUserId(userId)) {
            groups.add(Group.findByName(connection.getGroupName()));
        }
        return groups.toArray(new Group[0]);
    }

    /**
     * Метод проверки, входит ли пользователь в группу
     *
     * @param userId    - значение userId
     * @param groupName - значение groupName
     * @return булевое значение, входит ли пользователь в группу
     */
    public static boolean UserIsInGroup(int userId, String groupName) {
        return getUserToGroup(userId, groupName) != null;
    }

    /**
     * Метод создания записи в таблице
     *
     * @param userId    - значение user_id
     * @param groupName - значение group_name
     * @return булевое значение об успешности добавления
     */
    public static boolean create(int userId, String groupName) {
        if (UserIsInGroup(userId, groupName)) {
            return true;
        } else {
            executeWithOutRs(String.format("INSERT INTO %s (%s) VALUES(%s, '%s')", TABLE_NAME, COLUMN_NAMES_STRING, userId, groupName));
            return UserIsInGroup(userId, groupName);
        }
    }

    /**
     * Метод удаления записи в таблице
     *
     * @return булевое значение об успешности удаления
     */
    public boolean destroy() {
        ConnectionDB.executeWithOutRs(String.format("DELETE FROM %s WHERE user_id = %s and group_name = '%s'", TABLE_NAME, userId, groupName));
        return !UserIsInGroup(userId, groupName);
    }

    /**
     * метод получения одного экземпляра UserToGroup по условию поиска в таблице
     *
     * @param condition - условие поиска в таблице
     * @return экземпляр UserToGroup или null, если по запросу ничего не найдено
     */
    private static UserToGroup executeSelectOneInstance(String condition) {
        UserToGroup[] UserToGroupInList = executeSelect(condition);
        return (UserToGroupInList.length > 0) ? UserToGroupInList[0] : null;
    }

    /**
     * метод получения массива экземпляров UserToGroup по условию поиска в таблице
     *
     * @param condition - условие поиска в таблице
     * @return массив экземпляров UserToGroup
     */
    private static UserToGroup[] executeSelect(String condition) {
        ArrayList<UserToGroup> UserToGroups = connDB.executeSelect(generateSelectQuery(condition), modelExtractor);
        return UserToGroups.toArray(new UserToGroup[0]);
    }

    /**
     * метод генерации SQL-запроса для выборки из таблицы
     *
     * @param condition - условие поиска в таблице
     * @return строка с SQL-запросом в БД
     */
    private static String generateSelectQuery(String condition) {
        return String.format("SELECT %s FROM %s WHERE ", COLUMN_NAMES_STRING, TABLE_NAME) + condition;
    }

    /**
     * метод получения информации об экземпляре UserToGroup в строковом виде
     *
     * @return строка с информацией об экземпляре UserToGroup
     */
    @Override
    public String toString() {
        return String.format("%s - %s", groupName, User.findById(userId).toString());
    }

}
