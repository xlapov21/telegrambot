package telegrambot.bot.models;

import telegrambot.bot.lib.ConnectionDB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Класс - модель данных для таблицы pinned_files
 */
public class PinnedFile {

    /**
     * Экземпляр ConnectionDB
     */
    private static final ConnectionDB<PinnedFile> connDB = new ConnectionDB<>();

    /**
     * Лямбда-функция для создания экземпляра PinnedFile
     */
    private static final ConnectionDB.ModelExtractor<PinnedFile> modelExtractor = (rs) -> {
        PinnedFile file = null;
        try {
            file = new PinnedFile(
                    rs.getInt("id"),
                    rs.getString("title"),
                    rs.getString("file_id"),
                    rs.getString("user_chat_id")
            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return file;
    };

    /**
     * экземпляр PreparedStatement для обновления строк PinnedFile в БД
     */
    private final PreparedStatement stmtUpdate = ConnectionDB.getPreparedStatement(
            String.format("UPDATE %s SET %s = ? WHERE id = ?", TABLE_NAME, String.join(" = ?, ", COLUMN_NAMES)));

    /**
     * Имя таблицы, соответствующей классу PinnedFile
     */
    private static final String TABLE_NAME = "pinned_files";

    /**
     * Массив часто используемых названий колонок, соответствующих таблице pinned_files
     */
    private static final String[] COLUMN_NAMES = new String[]{
            "title",
            "file_id",
            "user_chat_id"
    };

    /**
     * Строка с именами колонок
     */
    private static final String ALL_COLUMN_NAMES_STRING = "id, " + String.join(", ", COLUMN_NAMES);

    /**
     * ID файла из pinned_files.id
     */
    private int id;

    /**
     * ID файла из pinned_files.file_id
     */
    private String fileId;

    /**
     * Название файла из pinned_files.title
     */
    private final String title;

    /**
     * ID чата пользователя, закрепившего (последнего обновившего) этот файл, из users.chat_id
     */
    private String userChatId;

    /**
     * Геттер id
     *
     * @return значение id
     */
    public int getId() {
        return id;
    }

    /**
     * Геттер title
     *
     * @return значение title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Геттер fileId
     *
     * @return значение fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Геттер userChatId
     *
     * @return значение userChatId
     */
    public String getUserChatId() {
        return userChatId;
    }

    /**
     * Сеттер fileId
     *
     * @param fileId - новое значение fileId
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * Сеттер userChatId
     *
     * @param userChatId - новое значение userChatId
     */
    public void setUserChatId(String userChatId) {
        this.userChatId = userChatId;
    }

    /**
     * Конструктор PinnedFile
     *
     * @param id         значение id
     * @param title      значение title
     * @param fileId     значение fileId
     * @param userChatId значение userChatId
     */
    public PinnedFile(int id, String title, String fileId, String userChatId) {
        this.id = id;
        this.title = title;
        this.fileId = fileId;
        this.userChatId = userChatId;
    }

    /**
     * метод добавления в БД новой строки
     *
     * @param title      - значение столбца title
     * @param fileId     - значение столбца file_id
     * @param userChatId - значение столбца user_chat_id
     * @return сгенерированный id или -1, если строка не создалась
     */
    public static int create(
            String title,
            String fileId,
            String userChatId
    ) {
        PreparedStatement stmtCreate = ConnectionDB.getPreparedStatement(
                String.format("INSERT INTO %s (%s) VALUES(?, ?, ?)", TABLE_NAME, String.join(", ", COLUMN_NAMES)),
                new String[]{"id"}
        );
        int newId = -1;
        try {
            setValues(
                    stmtCreate,
                    title,
                    fileId,
                    userChatId
            );
            stmtCreate.executeUpdate();
            ResultSet gk = stmtCreate.getGeneratedKeys();
            if (gk.next()) {
                newId = gk.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return newId;
    }

    /**
     * метод сохранения экземпляра PinnedFile в БД
     *
     * @return булево значение об успешности выполнения
     */
    public boolean save() {
        if (id > 0) {
            try {
                stmtUpdate.clearParameters();
                setValues(
                        stmtUpdate,
                        title,
                        fileId,
                        userChatId
                );
                stmtUpdate.setInt(4, id);
                stmtUpdate.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            PinnedFile file = findById(id);
            return file != null &&
                    file.getId() == id &&
                    file.getTitle().equals(title) &&
                    file.getFileId().equals(fileId) &&
                    file.getUserChatId().equals(userChatId);
        } else {
            id = create(
                    title,
                    fileId,
                    userChatId
            );
            return id > 0;
        }
    }

    /**
     * Вспомогательный метод для установки значений в экземпляре PreparedStatement
     *
     * @param stmt       - экземпляр PreparedStatement
     * @param title      - значение столбца title
     * @param fileId     - значение столбца file_id
     * @param userChatId - значение столбца user_chat_id
     */
    private static void setValues(
            PreparedStatement stmt,
            String title,
            String fileId,
            String userChatId
    ) throws SQLException {
        stmt.setString(1, title);
        stmt.setString(2, fileId);
        stmt.setString(3, userChatId);
    }

    /**
     * метод удаления экземпляра PinnedFile из БД
     *
     * @return булево значение об успешности выполнения
     */
    public boolean destroy() {
        ConnectionDB.executeWithOutRs(String.format("DELETE FROM %s WHERE id = %s", TABLE_NAME, id));
        return findById(id) == null;
    }

    /**
     * метод получения массива из всех экземпляров PinnedFile в таблице
     *
     * @return массив экземпляров PinnedFile
     */
    public static PinnedFile[] all() {
        return executeSelect("true");
    }

    /**
     * Метод получения экземпляра PinnedFile по id
     *
     * @param id - значение столбца id
     * @return экземпляр PinnedFile, где id == id (аргумент)
     */
    public static PinnedFile findById(int id) {
        return executeSelectOneInstance("id = " + id);
    }

    /**
     * Метод получения экземпляра PinnedFile по title
     *
     * @param title - значение столбца title
     * @return экземпляр PinnedFile, где title == title (аргумент)
     */
    public static PinnedFile findByTitle(String title) {
        return executeSelectOneInstance(String.format("title = '%s'", title));
    }

    /**
     * метод получения одного экземпляра PinnedFile по SQL-запросу
     *
     * @return возвращает экземпляр PinnedFile или null, если по запросу ничего не найдено
     */
    private static PinnedFile executeSelectOneInstance(String condition) {
        PinnedFile[] userInList = executeSelect(condition);
        return (userInList.length > 0) ? userInList[0] : null;
    }

    /**
     * метод получения массива экземпляров PinnedFile по условию поиска в таблице
     *
     * @param condition условие поиска в таблице
     * @return массив экземпляров PinnedFile
     */
    private static PinnedFile[] executeSelect(String condition) {
        ArrayList<PinnedFile> files = connDB.executeSelect(generateSelectQuery(condition), modelExtractor);
        return files.toArray(new PinnedFile[0]);
    }

    /**
     * метод генерации SQL-запроса для выборки из таблицы
     *
     * @param condition условие поиска в таблице
     * @return строка с SQL-запросом в БД
     */
    private static String generateSelectQuery(String condition) {
        return String.format("SELECT %s FROM %s WHERE %s", ALL_COLUMN_NAMES_STRING, TABLE_NAME, condition);
    }

    @Override
    public String toString() {
        return String.format("%s. %s", id, title);
    }
}
