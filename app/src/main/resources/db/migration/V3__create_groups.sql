CREATE TABLE IF NOT EXISTS groups
(
    id        integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name      text UNIQUE NOT NULL,
    title     text NOT NULL,
    is_closed boolean,
    is_hidden boolean
);