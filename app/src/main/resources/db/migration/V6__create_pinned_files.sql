CREATE TABLE IF NOT EXISTS pinned_files
(
    id           int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title        text NOT NULL UNIQUE,
    file_id      text NOT NULL UNIQUE,
    user_chat_id text REFERENCES users (chat_id)
);