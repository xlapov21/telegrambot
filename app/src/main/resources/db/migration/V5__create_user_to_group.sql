CREATE TABLE IF NOT EXISTS user_to_group
(
    user_id    int REFERENCES users ON DELETE CASCADE          NOT NULL,
    group_name text REFERENCES groups (name) ON DELETE CASCADE NOT NULL,
    UNIQUE (user_id, group_name)
);