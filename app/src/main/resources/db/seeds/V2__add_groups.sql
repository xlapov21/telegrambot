INSERT INTO groups (name, title, is_closed, is_hidden)
VALUES ('7.1', 'Информационные системы и программирование', true, true),
       ('7.2', 'Информационные системы и программирование', true, true),
       ('6.1', 'Сетевое и системное администрирование', true, true),
       ('1.1', 'Компьютерные системы и комплексы', true, true),
       ('Ц', 'Центуриат', true, true),
       ('А', 'Абитуриенты', false, false),
       ('С', 'Сотрудники', true, false),
       ('Ст', 'Студенты', true, false),
       ('IT_round', 'Участники IT-раунда', true, false)
;